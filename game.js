var game = new Phaser.Game(800, 600, Phaser.AUTO, '', { preload: preload, create: create, update:update });

function preload() {
    game.load.image('bullet', 'assets/bullet.png');
    game.load.image('invader', 'assets/enemy.png');
    game.load.image('einstein', 'assets/ship.png');
    game.load.spritesheet('kaboom', 'assets/explosion.png', 64, 64);
    game.load.audio('pew', 'assets/pew.wav');
    game.load.audio('explode', 'assets/explode.wav');
}

var player;
var cursors;
var bullets;
var bulletTime = 0;
var fireButton;
var aliens;
var explosions;
var pewSound;
var explodeSound;

function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    player = game.add.sprite(0, 0, 'einstein');
    player.anchor.setTo(0.5, 0.5);
    player.position.set(200, 200);
    game.physics.enable(player, Phaser.Physics.ARCADE);

    pewSound = game.add.audio('pew');
    explodeSound = game.add.audio('explode');

    //  Our bullet group
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(30, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);

    //  An explosion pool
    explosions = game.add.group();
    explosions.createMultiple(30, 'kaboom');
    explosions.forEach(setupInvader, this);

    aliens = game.add.group();
    aliens.enableBody = true;
    aliens.physicsBodyType = Phaser.Physics.ARCADE;

    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    createEnemy();
}

function createEnemy() {
    var alien = aliens.create(Math.random() * 500, 50, 'invader');
    alien.anchor.setTo(0.5, 0.5);
}

function setupInvader (invader) {

    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');

}

function update() {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);

        if (cursors.left.isDown)
        {
            player.body.velocity.x = -200;
        }
        else if (cursors.right.isDown)
        {
            player.body.velocity.x = 200;
        }
        if (cursors.up.isDown)
        {
            player.body.velocity.y = -200;
        }
        else if (cursors.down.isDown)
        {
            player.body.velocity.y = 200;
        }

        //  Firing?
        if (fireButton.isDown)
        {
            fireBullet();
        }

        /*if (game.time.now > firingTimer)
        {
            enemyFires();
        }*/

        //  Run collision
        game.physics.arcade.overlap(bullets, aliens, collisionHandler, null, this);
        //game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);
}

function fireBullet () {

    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime)
    {
        //  Grab the first bullet we can from the pool
        var bullet = bullets.getFirstExists(false);

        if (bullet)
        {
            //  And fire it
            bullet.reset(player.x, player.y - 8);
            bullet.body.velocity.y = -400;
            bulletTime = game.time.now + 200;
            bullet.events.onOutOfBounds.add(resetBullet, this);

            pewSound.play();
        }
    }

}

function resetBullet (bullet) {
    //  Called if the bullet goes out of the screen
    bullet.kill();

}

function collisionHandler (bullet, alien) {

    //  When a bullet hits an alien we kill them both
    bullet.kill();
    alien.kill();

    //  Increase the score
    //score += 20;
    ///scoreText.text = scoreString + score;

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(alien.body.x + alien.width / 2, alien.body.y + alien.height / 2);
    explosion.anchor.setTo(0.5, 0.5);
    explosion.play('kaboom', 10, false, true);
    explodeSound.play();

    setTimeout(createEnemy, 250);

    /*if (aliens.countLiving() == 0)
    {
        score += 1000;
        scoreText.text = scoreString + score;

        enemyBullets.callAll('kill',this);
        stateText.text = " You Won, \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }*/
}